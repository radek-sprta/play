#! /usr/bin/env python3

# Script to play url to video in mpv
#
# Usage: play.py | play.py URL
#
# Author: Radek Sprta <mail@radeksprta.eu>
# Date: 2017-04-13
# License: The MIT License
import getopt
import sys
import subprocess

import pyperclip


acceleration = True


def get_opts():
    """
    Gets command line arguments.

    Returns:
        (str): Options to pass to mpv.
    """
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'N', ['no-acceleration'])
    except getopt.GetoptError as e:
        print(e)
        sys.exit(1)
    for opt, _ in opts:
        if opt in ('-N', '--no-acceleration'):
            global acceleration
            acceleration = False


def get_url():
    """
    Gets url from clipboard or commandline.
    """
    # TODO Make it take work even when command line arguments are given.
    if len(sys.argv) > 1 and get_opts() == '':
        # Get address from command line
        address = ''.join(sys.argv[1:])
    else:
        # Get address from clipboard
        address = pyperclip.paste()
    return address


def main():
    """
    Open url in mpv to play.
    """
    try:
        get_opts()
        if acceleration:
            subprocess.run(['mpv', get_url()])
        else:
            subprocess.run(['mpv', '--hwdec=no', '--vo=opengl', get_url()])
    except Exception as e:
        print('Mpv is not installed', e)

if __name__ == '__main__':
    main()
